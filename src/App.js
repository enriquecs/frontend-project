import './App.css';
import React, { useState, useEffect } from 'react';

const API_URL = `https://${process.env.REACT_APP_ALIAS_NAME}.${process.env.REACT_APP_ENVIRONMENT_NAME}.${process.env.REACT_APP_DOMAIN_NAME}`

function App() {

  const [posts, setPosts] = useState([]);
  useEffect(() => {
    fetch(`${API_URL}/api`, {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': API_URL,
        'Access-Control-Allow-Methods': 'GET,PUT,POST,DELETE,PATCH,OPTIONS',
        'Access-Control-Allow-Headers': 'Origin, X-Requested-With, Content-Type, Accept'
      }
   })
       .then((response) => response.json())
       .then((data) => {
          setPosts(data);
       })
       .catch((err) => {
          console.log(err);
       });
 }, []);

  
  return (
    <div className="App">
      <header className="App-header">
        <p>
          This is the message from the server: {posts.message}
        </p>
      </header>
    </div>
  );
}

export default App;